package com.example.animeapp.data.database.dao

import androidx.room.*
import com.example.animeapp.data.database.entities.AnimeDbEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface AnimesDao {

    @Query("SELECT * FROM animes")
    fun getAllFlow(): Flow<List<AnimeDbEntity>>

    @Query("SELECT * FROM animes")
    fun getAll(): List<AnimeDbEntity>

    @Query("SELECT * FROM animes WHERE animes.id = :id")
    fun getById(id: Long): AnimeDbEntity

    @Delete
    fun deleteMeme(anime: AnimeDbEntity)

    @Insert( onConflict = OnConflictStrategy.REPLACE )
    fun insertAll(animes: List<AnimeDbEntity>)
}