package com.example.animeapp.data.api.services

import com.example.animeapp.data.dto.AnimeServiceResponseDTO
import retrofit2.http.GET

interface AnimesService {
    @GET("top/anime")
    suspend fun doGetAnimesRequest(): AnimeServiceResponseDTO
}