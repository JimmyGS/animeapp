package com.example.animeapp.data.dto

data class AnimeServiceResponseDTO(
    val data: List<AnimeDTO>
) /*{
    data class Data(
        val animes: List<AnimeDTO>
    )
}*/
