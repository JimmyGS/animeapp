package com.example.animeapp.data.dto

import com.example.animeapp.data.database.entities.AnimeDbEntity
import com.example.animeapp.data.entities.Anime

data class AnimeDTO(
    val mal_id: String,
    val title: String,
    val score: Double,
    val images: Images
) {

    data class Images(
        val jpg: Jpg
    ) {
        data class Jpg(
            val image_url: String,
            val small_image_url: String,
            val large_image_url: String
        )
    }

    fun toEntity(): Anime =
        Anime(
            id = mal_id.toInt(),
            name = title,
            url_image = images.jpg.small_image_url,
            score = score
        )

    fun toDbEntity():AnimeDbEntity =
        AnimeDbEntity(
            mal_id.toInt(),
            title,
            images.jpg.small_image_url,
            score
        )
}
