package com.example.animeapp.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.animeapp.data.database.dao.AnimesDao
import com.example.animeapp.data.database.entities.AnimeDbEntity

@Database(
    entities = [
        AnimeDbEntity::class
    ],
    version = 1 // Cada que compilemos una versión con un modelo diferente, hay que incrementar
)
abstract class MyAppDatabase: RoomDatabase() {
    abstract fun animesDao(): AnimesDao

    companion object {
        private var INSTANCE: MyAppDatabase? = null

        @Synchronized
        fun getInstance(context: Context): MyAppDatabase {
            if (INSTANCE == null)
                INSTANCE = Room.databaseBuilder(
                    context,
                    MyAppDatabase::class.java,
                    "animes_db"
                )
//                    .fallbackToDestructiveMigration()
                    .allowMainThreadQueries()
                    .build()

            return INSTANCE !!
        }
    }
}