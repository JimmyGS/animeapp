package com.example.animeapp.data.entities

data class Anime(
    val id: Int,
    var name: String,
    var url_image: String,
    var score: Double
)
