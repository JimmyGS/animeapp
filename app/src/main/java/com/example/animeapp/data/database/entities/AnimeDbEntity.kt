package com.example.animeapp.data.database.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.animeapp.data.entities.Anime

@Entity(tableName = "animes")
data class AnimeDbEntity(
    @PrimaryKey
    val id: Int,
    var name: String,
    var url_image: String,
    var score: Double
) {
    fun toEntity(): Anime =
        Anime(id, name, url_image, score)
}
