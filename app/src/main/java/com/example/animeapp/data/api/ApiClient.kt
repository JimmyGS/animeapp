package com.example.animeapp.data.api

import com.example.animeapp.data.api.services.AnimesService
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ApiClient {
    private val retrofitClient: OkHttpClient = OkHttpClient()

    private val retrofitInstance = Retrofit.Builder()
        .client( retrofitClient )
        .baseUrl("https://api.jikan.moe/v4/")
        .addConverterFactory( GsonConverterFactory.create() )
        .build()

    fun animeService() = retrofitInstance.create( AnimesService::class.java )
}