package com.example.animeapp.repositories

import android.util.Log
import com.example.animeapp.data.Resource
import com.example.animeapp.data.api.ApiClient
import com.example.animeapp.data.database.dao.AnimesDao
import com.example.animeapp.data.entities.Anime
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*

class AnimeRepository(
    private val animesDao: AnimesDao
) {
    private val TAG = javaClass.simpleName

    fun getAnimes(): Flow<Resource<List<Anime>>> =
        flow {

            // Al iniciar la petición se emite un recurso de tipo Cargando
            emit(Resource.loading())

            // Recolectamos todos los elementos de la base de datos y los convertimos en entidades de la aplicación
            val flujoDB: Flow<Resource<List<Anime>>> = animesDao.getAllFlow().map {
                Resource.success(it.map { it.toEntity() })
            }.onEach { emit( it ) }

            try {
                updateAnimes()
            } catch (e: Exception) {
                emit(Resource.error("La petición falló", e))
            }

            emitAll( flujoDB )
        }

    suspend fun updateAnimes() {
        // 2: HACEMOS LA PETICIÓN A INTERNET
        val listaDeAnimesDeInternet = ApiClient.animeService().doGetAnimesRequest().data
        Log.d(TAG, "listaDeAnimessDeInternet => $listaDeAnimesDeInternet")
        // 3: GUARDAMOS LA INFORMACIÓN EN LOCAL
        // 3.1: CUANDO GUARDAMOS EN LA BASE DE DATOS, RECIBIMOS ACTUALIZACIONES EN FLOW
        animesDao.insertAll(
            listaDeAnimesDeInternet.map {
                it.toDbEntity()
            }
        )
    }
}