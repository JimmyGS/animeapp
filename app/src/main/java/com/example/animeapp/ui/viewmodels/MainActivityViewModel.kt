package com.example.animeapp.ui.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.animeapp.data.Resource
import com.example.animeapp.data.database.MyAppDatabase
import com.example.animeapp.data.entities.Anime
import com.example.animeapp.repositories.AnimeRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class MainActivityViewModel(application: Application): AndroidViewModel(application) {

    // Obtenemos la instancia de la base de datos
    private val myAppDatabase = MyAppDatabase.getInstance( application.applicationContext )

    // Pasamos el DAO de la base de datos al repositorio
    private val animeRepository = AnimeRepository(myAppDatabase.animesDao())

    private val _listaAnimes = MutableStateFlow<Resource<List<Anime>>> ( Resource.loading() )

    val listaAnimes: StateFlow<Resource<List<Anime>>> = _listaAnimes

    //swipe refresh
    private val _isRefreshing = MutableStateFlow(false)
    val isRefreshing: StateFlow<Boolean>
        get() = _isRefreshing.asStateFlow()

    fun updateList() {
        viewModelScope.launch {
            _listaAnimes.value = Resource.loading()

            _isRefreshing.emit(true)

            try {
                animeRepository.updateAnimes()
            } catch (e: Exception) {
                _listaAnimes.value = Resource.error("Error al actualizar", e)
            }

            _isRefreshing.emit(false)
        }
    }

    init {
        viewModelScope.launch {
            animeRepository.getAnimes().collect() {
                _listaAnimes.value = it
            }
        }
    }
}