package com.example.animeapp.ui.layouts

import androidx.compose.runtime.Composable
import com.example.animeapp.data.entities.Anime
import com.example.animeapp.ui.components.Lista

@Composable
fun DashboardLayout(listaAnimes: List<Anime>, isRefreshing: Boolean, update: () -> Unit) {
    Lista(animes = listaAnimes, isRefreshing, update )
}