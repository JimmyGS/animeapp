package com.example.animeapp

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.animeapp.data.Resource
import com.example.animeapp.data.entities.Anime
import com.example.animeapp.ui.layouts.DashboardLayout
import com.example.animeapp.ui.theme.AnimeAppTheme
import com.example.animeapp.ui.viewmodels.MainActivityViewModel
import kotlinx.coroutines.flow.collect

class MainActivity : ComponentActivity() {

    private val mainActivityViewModel: MainActivityViewModel by viewModels<MainActivityViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val lista = remember {
                mutableStateOf<List<Anime>>( listOf() )
            }

            val error = remember {
                mutableStateOf( "" )
            }

            val cargando = remember {
                mutableStateOf( false )
            }

            //swipe refresh
            val isRefreshing by mainActivityViewModel.isRefreshing.collectAsState()
            
            LaunchedEffect(key1 = Unit) {
                mainActivityViewModel.listaAnimes.collect { recurso ->
                    cargando.value = false

                    when(recurso) {
                        is Resource.Error -> {
                            error.value = "${recurso.message} ${recurso.exception?.localizedMessage}"
                        }
                        is Resource.Loading -> {
                            cargando.value = true
                            error.value = ""
                        }
                        is Resource.Success -> {
                            lista.value = recurso.data
                        }
                    }

                }
            }

            AnimeAppTheme {
                Column() {

                    if (cargando.value) {
                        Text(text = "Cargando la información...")
                        Spacer(modifier = Modifier.height(10.dp))
                    }

                    if (error.value.isNotEmpty()) {
                        Text(text = error.value)
                        Spacer(modifier = Modifier.height(10.dp))
                    }

                    DashboardLayout(lista.value, isRefreshing) {
                        mainActivityViewModel.updateList()
                    }
                }
            }

        }
    }
}

